import { createSlice } from "@reduxjs/toolkit";
const initialState = {};
const BTDatVeCodeLaiSlice = createSlice({
  name: "btDatVeCodeLai",
  initialState,
  reducers: {},
});
export const {
  actions: btDatVeCodeLaiActions,
  reducer: btDatVeCodeLaiReducer,
} = BTDatVeCodeLaiSlice;
