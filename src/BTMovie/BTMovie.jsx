//rafce
import React, { useState } from "react";
import movieList from "./data.json";
import {
  useNavigate,
  generatePath,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import { PATH } from "../config/path";
import { generateSearchString } from "../utils/generateSearchString";
const BTMovie = () => {
  const { pathname } = useLocation();
  console.log("pathname :", pathname);
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = Object.fromEntries(searchParams);
  const [searchValue, setSearchValue] = useState();
  console.log("searchValue :", searchValue);

  const findMovieList = () => {
    const data = movieList.filter((item) =>
      item.tenPhim.toLocaleLowerCase().includes(searchQuery.movieName)
    );
    return data;
  };
  const renderData = (arrMovie) => {
    return arrMovie.map((movie) => {
      const path = generatePath(PATH.movieDetail, {
        movieId: movie.maPhim,
      });
      return (
        <div key={movie.maPhim} className="col-3 mt-3">
          <div className="card">
            <img
              style={{ width: "250px", height: "350px" }}
              src={movie.hinhAnh}
              alt="..."
            />
            <div
              className="card-body"
              style={{ height: "200px", overflow: "hidden" }}
            >
              <p className="font-weight-bold">{movie.tenPhim}</p>
              <p>{movie.moTa.substring(0, 50)}...</p>
              <button
                className="btn btn-outline-success my-3"
                onClick={() => {
                  navigate(path);
                }}
              >
                Detail
              </button>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="">
      <h1>BTMovie</h1>
      <div>
        <input
          placeholder="Tìm kiếm tên phim"
          className="form-control"
          defaultValue={searchQuery?.movieName}
          value={searchValue}
          onChange={(event) => {
            setSearchValue(event.target.value);
          }}
        />
        <button
          className="btn btn-success mt-3 align-left"
          onClick={() => {
            const searchString = generateSearchString({
              movieName: searchValue || undefined,
            });
            // setSearchParams({
            //   movieName: searchValue,
            // });

            navigate(pathname + searchString);
          }}
        >
          Tìm kiếm
        </button>
      </div>

      <div className="row">
        {/* {renderData(movieList)} */}
        {renderData(searchQuery?.movieName ? findMovieList() : movieList)}
      </div>
    </div>
  );
};

export default BTMovie;
