import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction } from "../store/BTDatVe/action";
import cn from "classnames";
import "./style.css";
const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  return (
    // lưu ý cú pháp
    <button
      className={cn("btn btn-outline-dark Chair", {
        // Tìm số ghế, nếu có số ghế chọn, thì sẽ xuất hiện thêm className booking
        booking: chairBookings.find((e) => e.soGhe == ghe.soGhe),
        booked: chairBookeds.find((e) => e.soGhe == ghe.soGhe),
      })}
      style={{ width: "50px", height: "50px" }}
      onClick={() => dispatch(chairBookingsAction(ghe))}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
