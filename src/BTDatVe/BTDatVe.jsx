import React from "react";
import data from "./data.json";
import Result from "./Result";
import ChairList from "./ChairList";
const BTDatVe = () => {
  return (
    <div className="container ">
      <h1>BTDatVe</h1>
      <div className="row">
        <div className="col-8">
          <div className="display-5">
            <h1>Đặt vé xem phim</h1>
            <div className="text-center p-3 bg-dark text-white mt-3">
              SCREEN
            </div>
            {/* danh sach ghe */}
            <ChairList data={data} />
          </div>
        </div>
        <div className="col-4">
          {/* ket qua dat ve */}
          <Result />
        </div>
      </div>
    </div>
  );
};

export default BTDatVe;
