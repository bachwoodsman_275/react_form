import React from "react";
import { useDispatch, useSelector } from "react-redux";
import "./style.css";
import { baiTapDatVeActions } from "../storeToolkit/BTDatVeToolkit/slice";
import cn from "classnames";

const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  // chỉ cần lấy chairBookings nên distructuring
  const { chairBookings, chairBooked } = useSelector(
    (state) => state.btDatVeToolkit
  );
  return (
    // lưu ý cú pháp, cái className này sẽ kiếm các phần tử sẵn có trong mảng để thay đổi
    <button
      className={cn("btn btn-outline-dark", {
        booking: chairBookings.find((e) => e.soGhe === ghe.soGhe),
        booked: chairBooked.find((e) => e.soGhe == ghe.soGhe),
      })}
      onClick={() => {
        dispatch(baiTapDatVeActions.setChairBookings(ghe));
      }}
      style={{ width: "50px", height: "50px" }}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
