import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction, payAction } from "../store/BTDatVe/action";
import { baiTapDatVeActions } from "../storeToolkit/BTDatVeToolkit/slice";

const Result = () => {
  const { chairBookings } = useSelector((state) => state.btDatVeToolkit);
  const dispatch = useDispatch();
  return (
    <div className="ml-5">
      <h2>Danh sách ghế bạn chọn</h2>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark booked">Ghế đã đặt</button>
        <button className="btn btn-outline-dark my-3 booking">
          Ghế đang chọn
        </button>
        <button className="btn btn-outline-dark">Ghế chưa đặt</button>
      </div>
      <table className="table">
        <thead>
          <th>Số ghế</th>
          <th>Giá</th>
          <th>Hủy</th>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className="btn btn-danger"
                  // tương tự như bấm vào btn ghế 2 lần
                  onClick={() => {
                    dispatch(baiTapDatVeActions.setChairBookings(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}
          {/* tính tổng tiền */}
          <tr>
            <td>Tổng tiền</td>
            <td>
              {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>
      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(baiTapDatVeActions.setChairBooked());
        }}
      >
        Thanh toán
      </button>
    </div>
  );
};

export default Result;
