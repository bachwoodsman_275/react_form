import React from "react";
import { NavLink } from "react-router-dom";

const HomePage = () => {
  return (
    <div className="container display-4 mt-5 d-flex flex-column">
      <NavLink to="/BTDatVe">ĐẶT VÉ</NavLink>
      <NavLink to="/BTDatVeToolkit">ĐẶT VÉ DÙNG TOOLKIT</NavLink>
    </div>
  );
};

export default HomePage;
