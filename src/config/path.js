export const PATH = {
  btDatVe: "/BTDatVe",
  btDatVeToolkit: "/redux/BTDatVeToolkit",
  btmovie: "/btmovie",
  movieDetail: "/movie/:movieId",
  notFound: "/notFound",
  btForm: "/btform",
};
