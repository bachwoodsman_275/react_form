import React from "react";
import SideBar from "../components/SideBar";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div className="row">
      {/* side bar */}
      <div className="col-2">
        <SideBar />
      </div>
      {/* content */}
      <div className="col-10">
        <Outlet />
      </div>
    </div>
  );
};

export default MainLayout;
