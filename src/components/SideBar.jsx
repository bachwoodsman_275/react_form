import React from "react";
import { PATH } from "../config/path";
import { NavLink } from "react-router-dom";
import "./style.scss";
const SideBar = () => {
  return (
    <div className="d-flex flex-column">
      <NavLink end to={PATH.btDatVe}>
        ĐẶT VÉ
      </NavLink>
      <NavLink to={PATH.btDatVeToolkit}>ĐẶT VÉ DÙNG TOOLKIT</NavLink>
      <NavLink to={PATH.btmovie}>XEM PHIM</NavLink>
      <NavLink to={PATH.btForm}>XEM FORM</NavLink>
    </div>
  );
};

export default SideBar;
