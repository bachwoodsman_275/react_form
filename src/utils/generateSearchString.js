/**
 *
 * @param: object
 * @return - search string dùng để query URL
 */

import qs from "qs";

export const generateSearchString = (params) => {
  const searchString = qs.stringify(params, {
    addQueryPrefix: true,
  });

  return searchString;
};
