import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  {
    return data.map((hangGhe) => {
      return (
        <div className="d-flex ">
          <div className=" ml-3 my-3">{hangGhe.hang}</div>
          <div className="d-flex flex-row ml-3 w-100">
            {hangGhe.danhSachGhe.map((ghe) => {
              return <Chair key={ghe.soGhe} ghe={ghe} />;
            })}
          </div>
        </div>
      );
    });
  }
};

export default ChairList;
