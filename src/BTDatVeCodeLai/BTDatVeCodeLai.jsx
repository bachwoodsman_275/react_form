import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Chair from "./Chair";
const BTDatVeCodeLai = () => {
  return (
    <div className="container">
      <h1>BTDatVeCodeLai</h1>
      <div className="row">
        <div className="col-8 mt-5">
          <div className="display-5">
            <h3>Đặt vé xem phim</h3>
            <h5 className="mt-3">SCREEN</h5>
            <div className="d-flex flex-column bg-dark mt-3 text-white">
              <ChairList data={data} />
            </div>
          </div>
        </div>
        <div className="col-4"></div>
      </div>
    </div>
  );
};

export default BTDatVeCodeLai;
