import logo from "./logo.svg";
import "./App.css";
import BTDatVe from "./BTDatVe/BTDatVe";
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import About from "./pages/About";
import BTDatVeToolkit from "./BTDatVeReduxToolkit/BTDatVeToolkit";
import BTDatVeCodeLai from "./BTDatVeCodeLai/BTDatVeCodeLai";
import { PATH } from "./config/path";
import MainLayout from "./layouts/MainLayout";
import BTMovie from "./BTMovie/BTMovie";
import MovieDetail from "./BTMovie/MovieDetail";
import NotFound from "./pages/NotFound";
import BTForm from "./BTForn/BTForm";
function App() {
  return (
    <div className="App">
      {/* <BTDatVeCodeLai /> */}
      <Routes>
        <Route element={<MainLayout />}>
          <Route index element={<BTDatVe />} />

          <Route path={PATH.btDatVe} element={<BTDatVe />} />
          <Route path={PATH.btDatVeToolkit} element={<BTDatVeToolkit />} />
          <Route path={PATH.btmovie} element={<BTMovie />} />
          <Route path={PATH.movieDetail} element={<MovieDetail />} />
          {/* Nested route */}
          {/* <Route path={PATH.redux}">
          <Route index element={<BTDatVe />} />
          <Route path={PATH.btDatVeToolkit} element={<BTDatVeToolkit />} />
        </Route> */}
          <Route path={PATH.btForm} element={<BTForm />}></Route>
        </Route>
        <Route path="*" element={<NotFound />}></Route>
      </Routes>
    </div>
  );
}

export default App;
