import { HANDLE_CHAIR_BOOKINGS, PAY } from "./actionType";

const initialState = { chairBookings: [], chairBookeds: [] };

export const btDatveReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case HANDLE_CHAIR_BOOKINGS: {
      const data = [...state.chairBookings];
      // kiểm tra ghế được click nó đã tồn tại trong mảng hay không
      const index = data.findIndex((value) => value.soGhe == payload.soGhe);

      if (index == -1) {
        data.push(payload);
      } else {
        data.splice(index, 1);
      }
      return { ...state, chairBookings: data };
    }
    case PAY: {
      // ném tất cả các thông tin vào mảng chairBookeds và reset thông tin mảng cũ
      const data = [...state.chairBookeds, ...state.chairBookings];
      return { ...state, chairBookeds: data, chairBookings: [] };
    }
    default:
      return state;
  }
};
