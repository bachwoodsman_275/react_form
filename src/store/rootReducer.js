import { combineReducers } from "redux";
import { btDatveReducer } from "./BTDatVe/reducer";

export const rootReducer = combineReducers({
  btDatVe: btDatveReducer,
});
