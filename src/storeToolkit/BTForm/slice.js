import { createSlice } from "@reduxjs/toolkit";

const initialState = { productList: [] };
const btSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
  },
});
export const { actions: btFormActions, reducer: btFormReducer } = btSlice;
