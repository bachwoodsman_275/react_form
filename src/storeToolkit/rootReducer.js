import { combineReducers } from "redux";
import { btDatveReducer } from "../store/BTDatVe/reducer";
import { baiTapDatVeReducer } from "./BTDatVeToolkit/slice";
import { btFormReducer } from "./BTForm/slice";

export const rootReducer = combineReducers({
  // reducer bài tập cũ
  btDatVe: btDatveReducer,
  // reducer bài tập redux toolkit
  btDatVeToolkit: baiTapDatVeReducer,
  btForm: btFormReducer,
});

// reducer
