import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  chairBookings: [],
  chairBooked: [],
};
const BTDatVeSlice = createSlice({
  name: "btDatVe",
  initialState,
  reducers: {
    setChairBookings: (state, action) => {
      // kiểm tra số ghế trong mảng có bằng số ghế truyền vào không
      const index = state.chairBookings.findIndex(
        (e) => e.soGhe === action.payload.soGhe
      );
      if (index !== -1) {
        state.chairBookings.splice(index, 1);
      } else {
        // nếu index == -1
        // push ghế vào mảng
        state.chairBookings.push(action.payload);
      }
      console.log("index :", index);
    },
    setChairBooked: (state, { payload }) => {
      state.chairBooked = [...state.chairBookings, ...state.chairBooked];
      state.chairBookings = [];
    },
  },
});
export const { actions: baiTapDatVeActions, reducer: baiTapDatVeReducer } =
  BTDatVeSlice;
