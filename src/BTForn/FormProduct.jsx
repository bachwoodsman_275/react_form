import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { btFormActions } from "../storeToolkit/BTForm/slice";

const FormProduct = () => {
  // button nằm trong thẻ form nếu ko gắn thuộc tính type => mặc định nó sẽ là submit

  const [formData, setFormData] = useState();
  console.log("formData: ", formData);

  const [formError, setFormError] = useState();
  console.log("formError: ", formError);

  const dispatch = useDispatch();

  // const handleFormData = (name, value) => {
  //     setFormData({
  //         ...formData,
  //         [name]: value,
  //     })
  // }

  // currying function
  const handleFormData = () => (ev) => {
    const { name, value, title, minLength } = ev.target;
    // console.log('minLength: ', minLength)
    // id, image

    let mess; // undefined
    if (minLength !== -1 && !value.length) {
      mess = `Vui lòng nhập thông tin ${title}`;
    } else if (value.length < minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
    }

    // console.log('mess: ', mess)
    setFormError({
      ...formError,
      [name]: mess,
    });

    setFormData({
      ...formData,
      [name]: mess ? undefined : value,
    });
  };

  return (
    <form
      onSubmit={(event) => {
        // ngăn sự kiện re load khi submit form
        event.preventDefault();

        console.log("Submit");
        dispatch(btFormActions.addProduct(formData));
      }}
      noValidate
    >
      <h2 className="px-2 py-4 bg-dark text-warning">Product Info</h2>
      <div className="form-group row">
        <div className="col-6">
          <p>ID</p>
          <input
            type="text"
            className="form-control"
            name="id"
            title="id"
            minLength={6}
            // required
            // onChange={(ev) => {
            //     setFormData({
            //         ...formData,
            //         id: ev.target.value,
            //     })
            // }}
            // onChange={(ev) => {
            //     handleFormData('id', ev.target.value)
            // }}
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.id}</p>
        </div>
        <div className="col-6">
          <p>Image</p>
          <input
            type="text"
            className="form-control"
            name="image"
            onChange={handleFormData()}
          />
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p>Name</p>
          <input
            type="text"
            className="form-control"
            name="name"
            onChange={handleFormData()}
          />
        </div>
        <div className="col-6">
          <p>Product Type</p>
          <input
            type="text"
            className="form-control"
            name="productType"
            onChange={handleFormData()}
          />
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p>Price</p>
          <input
            type="text"
            className="form-control"
            name="price"
            onChange={handleFormData()}
          />
        </div>
        <div className="col-6">
          <p>Description</p>
          <input
            type="text"
            className="form-control"
            name="description"
            onChange={handleFormData()}
          />
        </div>
      </div>
      <div className="mt-3">
        <button className="btn btn-success">Create</button>
        <button className="btn btn-info ml-3">Update</button>
      </div>
    </form>
  );
};

export default FormProduct;
