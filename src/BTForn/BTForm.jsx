import React from "react";
import FormProduct from "./FormProduct";
import TableProducts from "./TableProducts";
const BTForm = () => {
  return (
    <div>
      <h1>BTForm</h1>
      <FormProduct />
      <TableProducts />
    </div>
  );
};

export default BTForm;
