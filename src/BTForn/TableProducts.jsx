import React from "react";
import { useSelector } from "react-redux";

const TableProducts = () => {
  const { productList } = useSelector((state) => state.btForm);
  console.log("productList :", productList);
  return (
    <div className="mt-3">
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Type</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {productList.map((prd) => (
            <tr key={prd.id}>
              <td>{prd.id}</td>
              <td>{prd.image}</td>
              <td>{prd.name}</td>
              <td>{prd.price}</td>
              <td>{prd.description}</td>
              <td>{prd.productType}</td>
              <td>
                <button className="btn btn-success">Delete</button>
                <button className="btn btn-info ml-3">Edit</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableProducts;
